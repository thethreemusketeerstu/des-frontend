import fetch from 'isomorphic-fetch';

function checkStatus(response) {
	return new Promise((resolve,reject) =>{
		if (response.status >= 200 && response.status < 300) {
		    resolve(response)
		  } else {
		    var error = new Error(response.statusText)
		    error.response = response
		    reject(error)
		  }
	})
}


class Client {


	async login(email,password){
		try {
			let response = await fetch('http://localhost:8080/client/login',{
				method: 'POST',
	  			headers: {
	    				'Content-Type': 'application/json'
	  			},
	  			body: JSON.stringify({
	    			email: email,
					password: password
	  			})
			})
			let status = await checkStatus(response)
			let parsed = await status.json()
	    	console.log('request succeeded with JSON response', parsed.token)
	    	sessionStorage.token=parsed.token
				return parsed.token
	  	}catch(error){
	  		console.log('request failed', error)
	  	}
	}

	async tokenRefresh(){
		try {
			let token = sessionStorage.getItem('token')
			let response = await fetch('http://localhost:8080/client/tokenRefresh',{
				method: 'POST',
	  			headers: {
	    				'Content-Type': 'application/json'
	  			},
	  			body: JSON.stringify({token: token})
			})
			let status = await checkStatus(response)
			let parsed = await status.json()
	    	console.log('request succeeded with JSON response', parsed)
	    	this.token = parsed.token;
	  	}catch(error){
	  		console.log('request failed', error)
	  	}
	}

	async getDevices(){
		try {
			let token_storage = sessionStorage.getItem('token')
			let response = await fetch('http://localhost:8080/client/devices',{
				method: 'GET',
	  			headers: {
							'Content-Type': 'application/json',
	    				'Authorization': token_storage
	  			}
			})
			let status = await checkStatus(response)
			let parsed = await status.json()
			console.log(parsed)
	    	return parsed
	  	}catch(error){
	  		return error
	  	}
	}

	async getDeviceAvarageData(deviceid,fromDate,throughDate){
		try {
			let token_storage = sessionStorage.getItem('token')
			let response = await fetch(`http://localhost:8080/client/devices/${deviceid}/data?fromDate=${fromDate}&throughDate=${throughDate}`,{
				method: 'GET',
	  			headers: {
							'Content-Type': 'application/json',
	    				'Authorization': token_storage
	  			}
			})
			let status = await checkStatus(response)
			let parsed = await status.json()
			console.log(parsed)
	    	return parsed
	  	}catch(error){
	  		return error
	  	}
	}


	async createDevice(id,name,description){
		try {
			console.log('[client] deviceName: '+ name + ' deviceId: ' + id + ' description: ' + description)
			let token_storage = sessionStorage.getItem('token')
			let response = await fetch(`http://localhost:8080/client/devices`,{
				method: 'POST',
	  			headers: {
	    				'Content-Type': 'application/json',
	    				'Authorization': token_storage
	  			},
	  			body: JSON.stringify({
	  				"id": id,
					"name": name,
					"description": description
				})
			})
			let status = await checkStatus(response)
			console.log('Status of submit request' + response.status)
			let parsed = await status.json()
	    	console.log('request succeeded with JSON response', parsed)
	    	//return response.status;
	  	}catch(error){
	  		console.log('request failed', error)
	  	}
	}

	async deleteDevice(id){
		try {
			console.log('[client deleteDevice]' + id)
			let token_storage = sessionStorage.getItem('token')
			let response = await fetch(`http://localhost:8080/client/devices/${id}`,{
				method: 'DELETE',
	  			headers: {
	    				'Authorization': token_storage
	  			}
			})
			let status = await checkStatus(response)
			let parsed = await status.json()
	    	console.log('request succeeded with JSON response', parsed)
	    	return parsed;
	  	}catch(error){
	  		console.log('request failed')
	  		console.log(error)
	  	}
	}

	async getCurrentData(id){
		try {
			let token_storage = sessionStorage.getItem('token')
			let response = await fetch(`http://localhost:8080/client/devices/${id}/data/current`,{
				method: 'GET',
	  			headers: {
	    				'Authorization': token_storage
	  			}
			})
			let status = await checkStatus(response)
			let parsed = await status.json()
	    	console.log('request succeeded with JSON response', parsed)
	    	return parsed
	  	}catch(error){
	  		console.log('request failed')
	  		console.log(error)
	  	}
	}


}
export default Client;
