import Client from './client';

const login = async function(email, password){
	return new Promise((resolve,reject) => {
		try{
			console.log('\n LOGIN:\n')
			let result = des.login(email, password) //thethreemusketeers@des.net
			resolve(result)
		}catch(err){
			reject(err)
		}
	})
}

const tokenRefresh = async function(){
	return new Promise((resolve,reject) => {
		try{
			console.log('\n REFRESH:\n')
			let result = des.tokenRefresh()
			resolve(result)
		}catch(err){
			reject(err)
		}
	})
}

const getDevices = async function(){
	return new Promise((resolve,reject) => {
		try{
			console.log('\n GET DEVICES:\n')
			let result = des.getDevices()
			resolve(result)
		}catch(err){
			reject(err)
		}
	})
}

const getDeviceAvarageData = async function(deviceid,fromDate,throughDate){
	return new Promise((resolve,reject) => {
		try{
			console.log('\n GET AVARAGE DATA:\n')
			let result = des.getDeviceAvarageData(deviceid,fromDate,throughDate)
			resolve(result)
		}catch(err){
			reject(err)
		}
	})
}


const deleteDevice = async function(deviceId){
	return new Promise((resolve,reject) => {
		try{
			console.log('\n DELETE DEVICE:\n')
			console.log('[main deviceId' + deviceId)
			let result = des.deleteDevice(deviceId)
			resolve(result)
		}catch(err){
			reject(err)
		}
	})
}

const createDevice = async function (deviceName, deviceId, description){
	return new Promise((resolve,reject) => {
		try{
			console.log('\n CREATE DEVICE:\n')
			console.log('[main] deviceName: '+ deviceName + ' deviceId: ' + deviceId + ' description: ' + description)
			let result = des.createDevice(deviceId, deviceName, description)
			//console.log(result)
			//console.log('[main]result ' + JSON.stringify(result));
			resolve(result)
		}catch(err){
			reject(err)
		}
	})
}

const getCurrentData = async function (id){
	return new Promise(async function(resolve,reject){
		try{
			console.log('\n GET CURRENT DATA:\n')
			let result = await des.getCurrentData(id)
			console.log('[main] gCD result' + JSON.stringify(result))
			resolve(result)
		}catch(err){
			reject(err)
		}
	})
}


let des = new Client();

export {
	login,
	getDevices,
	tokenRefresh,
	createDevice,
	deleteDevice,
	getCurrentData,
	getDeviceAvarageData
};
