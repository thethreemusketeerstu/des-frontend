export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
    },
    {
      name: 'Plot',
      url: '/plot',
      icon: 'icon-chart',
    },
  ],
};
