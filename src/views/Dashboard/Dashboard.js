import React, { Component } from 'react';
import DeviceWidget from '../../views/Widgets/DeviceWidget';
import AddDeviceForm from '../../views/Base/Forms/AddDeviceForm';
import propType from 'react';
import fetch from 'isomorphic-fetch';
import {getDevices} from '../../rest/des-backend-client/main'

class DevicesListComponent extends Component{
  constructor(props){
    super(props);

    this.state = {
      devices: []
    }
  }


  componentDidMount(){
    getDevices()
      .then(data => this.setState({devices: data.data}))
  }

  render() {
    const {devices} = this.state;
    return (
      <div>{
        devices.map((device,i)=>
          <div>
          <DeviceWidget
            key={i}
            className={device.name}
            header={device.name}
            mainText={device.description}
            smallText={"Device ID: "+device.id}
            deviceId = {device.id}
          />
          </div>
        )}
      </div>
    )
  }
}


class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);

    this.state = {
      dropdownOpen: false,
      radioSelected: 2,
    };
  }


  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }


  render() {
    return (
      <div>
        <AddDeviceForm />
        <DevicesListComponent />
      </div>
    );
  }
}

export default Dashboard;
