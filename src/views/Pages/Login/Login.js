import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col, Container, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
//import  fakeAuth  from '../../../rest/auth';

import {login} from '../../../rest/des-backend-client/main'


class Login extends Component {
  constructor() {

    super();

    this.state = {
      redirectToReferrer: false,
      email:'thethreemusketeers@des.net',
      password:''
    }
    this.login_local = this.login_local.bind(this);
  }


  async login_local(){
      try{
        await login(this.state.email, this.state.password)
        this.setState({ redirectToReferrer: true })
      }catch(e){
        console.log(e)
      }

  }

  email(event) {
    this.setState({email: event.target.value})

  //console.log(this.state.email);
  }

  password(event) {
    this.setState({password: event.target.value})

  //console.log(this.state.password);
  }


  render() {
    const { from } = this.props.location.state || { from: { pathname: '/' } }
    const { redirectToReferrer } = this.state;

    if (redirectToReferrer) {
      return (
        <Redirect to={from} />
      )
    }

    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <h1>Login</h1>
                    <p className="text-muted">Sign In to your account</p>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-user"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" placeholder="Email" value ={this.state.email}
                        onChange={this.email.bind(this)}/>
                    </InputGroup>
                    <InputGroup className="mb-4">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="password" placeholder="Password" value ={this.state.password}
                        onChange={this.password.bind(this)}/>
                    </InputGroup>
                    <Row>
                      <Col xs="6">
                        <Button color="primary" onClick={this.login_local} className="px-4">Login</Button>
                      </Col>
                      <Col xs="6" className="text-right">
                        <Button color="link" className="px-0">Forgot password?</Button>
                      </Col>
                    </Row>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}


export default Login;
