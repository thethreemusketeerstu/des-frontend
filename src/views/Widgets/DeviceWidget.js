import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Card, CardBody, Table, Button} from 'reactstrap';
import classNames from 'classnames';
import { mapToCssModules } from 'reactstrap/lib/utils';
import {deleteDevice, getCurrentData} from '../../rest/des-backend-client/main'

const propTypes = {
  header: PropTypes.string,
  mainText: PropTypes.string,
  smallText: PropTypes.string,
  color: PropTypes.string,
  value: PropTypes.string,
  children: PropTypes.node,
  className: PropTypes.string,
  cssModule: PropTypes.object,
  variant: PropTypes.string,
};

const defaultProps = {
  header: '89.9%',
  mainText: 'Lorem ipsum...',
  smallText: 'Lorem ipsum dolor sit amet enim.',
  // color: '',
  value: '25',
  variant: '',
};

class DeviceWidget extends Component {

  constructor(){
    super();
    this.state = {
      deviceId:"",
      currentData:{
        'f': '0',
        'fromDate': '0',
        'i1': '0',
        'i2': '0',
        'i3': '0',
        'p': '0',
        'q': '0',
        'u1': '0',
        'u2': '0',
        'u3': '0'
      },
      deviceTest:"",
      fromDateParsed: 0
    }
    this.remove = this.remove.bind(this);
  }

  async componentDidMount() {
    /*
    console.log('Widget mounted')
    console.log('[componentDidMount]' + this.state.deviceId)
    let deviceId = this.props.deviceId;
    let initialData = await getCurrentData(deviceId)
    this.setState({currentData: initialData})
    let temp = this.state.currentData.fromDate.toString()
    this.setState({fromDateParsed: temp.replace("T", " ").replace("Z", " ")})

    const that = this
    let currentData = setInterval(async function(){
      that.setState({currentData: await getCurrentData(deviceId)})
      let temp = that.state.currentData.fromDate.toString()
      that.setState({fromDateParsed: temp.replace("T", " ").replace("Z", " ")})
    }, 30000);*/
  }

  componentWillUnmount(){
    clearInterval(this.currentDataRefresh);
  }

  async remove(id){
      try{
        await deleteDevice(id)
      }catch(e){
        console.log(e)
      }
  }

  render() {
    const { className, cssModule, header, mainText, smallText, deviceId, color, value, children, variant, ...attributes } = this.props;
    // demo purposes only
    const card = { style: '', bgColor: '' };

    if (variant === 'inverse') {
      card.style = 'text-white';
      card.bgColor = 'bg-' + color;
    }

    const classes = mapToCssModules(classNames(className, card.style, card.bgColor), cssModule);

    return (
      <Card className={classes} {...attributes}>
        <CardBody>
          <div className="h4 m-0">{header}</div>
          <div>{mainText}</div>
          <Table responsive>
            <thead>
            <tr>
              <th>Current1</th>
              <th>Current2</th>
              <th>Current3</th>
              <th>Voltage1</th>
              <th>Voltage2</th>
              <th>Voltage3</th>
              <th>Active Power</th>
              <th>Reactive Power</th>
              <th>Frequency</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td>{this.state.currentData.i1 + " A"}</td>
              <td>{this.state.currentData.i2 + " A"}</td>
              <td>{this.state.currentData.i3 + " A"}</td>
              <td>{this.state.currentData.u1 + " V"}</td>
              <td>{this.state.currentData.u2 + " V"}</td>
              <td>{this.state.currentData.u3 + " V"}</td>
              <td>{this.state.currentData.p + " W"}</td>
              <td>{this.state.currentData.q + " Var"}</td>
              <td>{this.state.currentData.f + " Hz"}</td>
            </tr>
            </tbody>
          </Table>
          <div className="container">
            <div className="row">
              <div className="col-sm">
                <div><small className="text-muted">{"Last updated at: " + this.state.fromDateParsed}</small></div>
                <div><small className="text-muted">{smallText}</small></div>
              </div>
              <div className="col-sm">
                <Button color="danger" onClick = {() => this.remove(deviceId)}>Remove</Button>
              </div>
            </div>
          </div>
          <div>{children}</div>
        </CardBody>
      </Card>
    );
  }
}

DeviceWidget.propTypes = propTypes;
DeviceWidget.defaultProps = defaultProps;

export default DeviceWidget;
