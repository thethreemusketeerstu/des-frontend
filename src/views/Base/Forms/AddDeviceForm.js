import React, { Component } from 'react';
import { Button, Form, FormGroup, Label, Input, Card, CardBody, CardTitle} from 'reactstrap';
import {createDevice} from '../../../rest/des-backend-client/main'

class AddDeviceForm extends Component {
  constructor(){
    super();
    this.state = {
      deviceName:"",
      deviceId:"",
      description:""
    }
    this.submit = this.submit.bind(this);
  }

  deviceName(event) {
    this.setState({deviceName: event.target.value})

  //console.log(this.state.deviceName);
  }

  deviceId(event) {
    this.setState({deviceId: event.target.value})

  //console.log(this.state.deviceId);
  }

  description(event) {
    this.setState({description: event.target.value})

  //console.log(this.state.description);
  }

  async submit(){
      try{
        await createDevice(this.state.deviceName, this.state.deviceId, this.state.description)
        //alert('success')
        //console.log('[STATUS]' + JSON.stringify.status);
        //this.setState({ redirectToReferrer: true })
      }catch(e){
        console.log(e)
        //alert('faulure')
      }
  }

  render() {
    return (
      <Card className="addDeviceCard">
        <CardBody>
          <CardTitle>Add new device</CardTitle>
          <Form>
            <FormGroup>
              <Label for="device">Device name</Label>
              <Input type="device" name="device" id="deviceInput" placeholder="Put device name here." value = {this.state.deviceName}
               onChange={this.deviceName.bind(this)}/>
            </FormGroup>
            <FormGroup>
              <Label for="deviceId">Device ID</Label>
              <Input type="deviceId" name="deviceId" id="deviceId" placeholder="Put device ID here." value = {this.state.deviceId}
               onChange={this.deviceId.bind(this)}/>
            </FormGroup>
            <FormGroup>
              <Label for="description">Device description</Label>
              <Input type="description" name="description" id="description " placeholder="Put device description here." value = {this.state.description}
               onChange={this.description.bind(this)}/>
            </FormGroup>
            <Button onClick = {this.submit}>Submit</Button>
          </Form>
      </CardBody>
      </Card>
    );
  }
}

export default AddDeviceForm;
