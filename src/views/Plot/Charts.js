import React, { Component } from 'react';
import { Card, CardBody} from 'reactstrap';
import { CardHeader } from 'reactstrap';
import { Line } from 'react-chartjs-2';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';

const options = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  maintainAspectRatio: false
}

class Charts extends Component {
  constructor(props) {
      super(props);
      this.state = {};
  }

  render() {
      return (
        <div>
        <Card>
          <CardHeader> Currents </CardHeader>
          <CardBody>
            <div className="chart-wrapper">
              <Line data={{
                labels: this.props.dataset.timestamp,
                datasets: [
                  {
                    label: 'Current 1',
                    borderColor: 'red',
                    data: this.props.dataset.i1
                  },
                  {
                    label: 'Current 2',
                    borderColor: 'green',
                    data: this.props.dataset.i2
                  },
                  {
                    label: 'Current 3',
                    borderColor: 'black',
                    data: this.props.dataset.i3
                  }
                ]
              }} options={options} />
            </div>
          </CardBody>
        </Card>
        <Card>
          <CardHeader> Voltages </CardHeader>
          <CardBody>
            <div className="chart-wrapper">
              <Line data={{
                labels: this.props.dataset.timestamp,
                datasets: [
                  {
                    label: 'Current 1',
                    borderColor: 'red',
                    data: this.props.dataset.u1
                  },
                  {
                    label: 'Current 2',
                    borderColor: 'green',
                    data: this.props.dataset.u2
                  },
                  {
                    label: 'Current 3',
                    borderColor: 'black',
                    data: this.props.dataset.u3
                  }
                ]
              }} options={options} />
            </div>
          </CardBody>
        </Card>
        <Card>
          <CardHeader> Real Power </CardHeader>
          <CardBody>
            <div className="chart-wrapper">
              <Line data={{
                labels: this.props.dataset.timestamp,
                datasets: [
                  {
                    label: 'Real Power',
                    borderColor: 'red',
                    data: this.props.dataset.p
                  }
                ]
              }} options={options} />
            </div>
          </CardBody>
        </Card>
        <Card>
          <CardHeader> Apparent Power </CardHeader>
          <CardBody>
            <div className="chart-wrapper">
              <Line data={{
                labels: this.props.dataset.timestamp,
                datasets: [
                  {
                    label: 'Apparent Power',
                    borderColor: 'black',
                    data: this.props.dataset.q
                  }
                ]
              }} options={options} />
            </div>
          </CardBody>
        </Card>
        <Card>
          <CardHeader> Frequency </CardHeader>
          <CardBody>
            <div className="chart-wrapper">
              <Line data={{
                labels: this.props.dataset.timestamp,
                datasets: [
                  {
                    label: 'Frequency',
                    borderColor: 'green',
                    data: this.props.dataset.f
                  }
                ]
              }} options={options} />
            </div>
          </CardBody>
        </Card>
        </div>
      );
  }
}

export default Charts;
