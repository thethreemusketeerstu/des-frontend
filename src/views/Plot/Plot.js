import React, { Component } from 'react';

import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';

import { DateRangePicker } from 'react-dates';
import { Card, CardBody, CardTitle, Dropdown, DropdownMenu, DropdownToggle, DropdownItem, Button, Table } from 'reactstrap';

import Charts from "./Charts"
import {getDevices,getDeviceAvarageData} from '../../rest/des-backend-client/main'

class DatasetTableWrapper extends Component {
  constructor(props){
    super(props);
    this.state = {};
  }
  render(){
    return(
      <Card className="datasetTable">
        <CardBody>
        <CardTitle>Full Dataset Table</CardTitle>
        <Table responsive>
          <thead>
          <tr>
            <th>Current1</th>
            <th>Current2</th>
            <th>Current3</th>
            <th>Voltage1</th>
            <th>Voltage2</th>
            <th>Voltage3</th>
            <th>Active Power</th>
            <th>Reactive Power</th>
            <th>Frequency</th>
            <th>Timestamp</th>
          </tr>
          </thead>
          <tbody>
            <DatasetTable dataset={this.props.dataset} />
          </tbody>
        </Table>
        </CardBody>
      </Card>
    );
  }
}

function DatasetTable(props) {
  const dataset = props.dataset;
  const listDataset = dataset.map((line,i)=>
    <tr key={i}>
      <td>{line.i1} A</td>
      <td>{line.i2} A</td>
      <td>{line.i3} A</td>
      <td>{line.u1} V</td>
      <td>{line.u2} V</td>
      <td>{line.u3} V</td>
      <td>{line.p}  W</td>
      <td>{line.q}  Var</td>
      <td>{line.f}  Hz</td>
      <td>{line.fromDate}</td>
    </tr>
  );
  return(listDataset);
}

class Plot extends Component {
    constructor(props) {
      super(props);
      this.state = {
        startDate: null,
        endDate: null,
        focusedInput: null,
        dropdownOpen: false,
        chosenDevice: null,
        devices: [],
        dataRequestStatus: false,
        showStatus: false,
        dataset:[],
        datasetChart:{
          timestamp: [],
          i1: [],
          i2: [],
          i3: [],
          u1: [],
          u2: [],
          u3: [],
          p: [],
          q: [],
          f: []
        },
        datasetChartReady: false
      };
      this.toggle = this.toggle.bind(this);
      this.onDatesChange = this.onDatesChange.bind(this);
      this.onClickDropdownItem = this.onClickDropdownItem.bind(this);
      this.requestData = this.requestData.bind(this);
    }

    componentDidMount(){
      getDevices()
      .then(data => this.setState({devices: data.data}))
    }

    onDatesChange({ startDate, endDate }) {
      console.log(startDate)
      this.setState({
        startDate: startDate,
        endDate: endDate,
      });
    }

    onClickDropdownItem(event){
      this.state.devices.map((device)=>{
        if(device.name === event.target.innerText)
          this.setState({chosenDevice: device})
      })
    }

    requestData(){
      if(this.state.startDate != null && this.state.endDate != null && this.state.chosenDevice != null){
        this.setState({showStatus: true})
        getDeviceAvarageData(this.state.chosenDevice.id,new Date(this.state.startDate).toISOString(),new Date(this.state.endDate).toISOString())
          .then((res)=>{
            this.setState({dataset:res.data,dataRequestStatus: true})
          })
          .then(()=>{
            let local_dataset = {
              timestamp: [],
              i1: [],
              i2: [],
              i3: [],
              u1: [],
              u2: [],
              u3: [],
              p: [],
              q: [],
              f: []
            }
            this.state.dataset.map(function(datapoint){
              local_dataset.timestamp.push(datapoint.fromDate)
              local_dataset.i1.push(datapoint.i1)
              local_dataset.i2.push(datapoint.i2)
              local_dataset.i3.push(datapoint.i3)
              local_dataset.u1.push(datapoint.u1)
              local_dataset.u2.push(datapoint.u2)
              local_dataset.u3.push(datapoint.u3)
              local_dataset.p.push(datapoint.p)
              local_dataset.q.push(datapoint.q)
              local_dataset.f.push(datapoint.f)
            })
            this.setState({datasetChart: local_dataset, datasetChartReady: true})
          })
      }else{
        console.log('kor')
      }
    }

    toggle() {
      this.setState(prevState => ({
        dropdownOpen: !prevState.dropdownOpen
      }));
    }

    render() {
      return (
        <div>
          <Card className="plotConfig">
              <CardBody>
                <CardTitle>Configure Plot</CardTitle>
                <div className="container">
                    <div className="row">
                        <div className="col-sm">
                            <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                              <DropdownToggle caret>
                                {this.state.chosenDevice !== null ? this.state.chosenDevice.name : 'Dropdown'}
                              </DropdownToggle>
                              <DropdownMenu>
                                {this.state.devices.map((device)=><DropdownItem key={device.name}onClick={this.onClickDropdownItem} enabled="True">{device.name}</DropdownItem>)}
                              </DropdownMenu>
                            </Dropdown>
                        </div>
                        <div className="col-sm">
                          <DateRangePicker
                            startDateId="startDate"
                            endDateId="endDate"
                            startDate={this.state.startDate}
                            endDate={this.state.endDate}
                            onDatesChange={this.onDatesChange}
                            focusedInput={this.state.focusedInput}
                            onFocusChange={(focusedInput) => { this.setState({ focusedInput })}}
                            isOutsideRange={() => false}
                          />
                        </div>
                        <div className="col-sm">
                          <Button color="info" onClick={this.requestData}>Submit</Button>
                        </div>
                    </div>
                </div>
              </CardBody>
          </Card>
          {this.state.datasetChartReady !== true ? <div></div> : <Charts dataset={this.state.datasetChart} />}
          {this.state.dataRequestStatus !== true ? <div></div> : <DatasetTableWrapper dataset={this.state.dataset} />}
        </div>
      );
    }
}

export default Plot;
