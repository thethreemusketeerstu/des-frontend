import React, { Component } from 'react';
import { HashRouter, Route, Switch ,Redirect} from 'react-router-dom';
import './App.css';
// Styles
// CoreUI Icons Set
import '@coreui/icons/css/coreui-icons.min.css';
// Import Flag Icons Set
import 'flag-icon-css/css/flag-icon.min.css';
// Import Font Awesome Icons Set
import 'font-awesome/css/font-awesome.min.css';
// Import Simple Line Icons Set
import 'simple-line-icons/css/simple-line-icons.css';
// Import Main styles for this application
import './scss/style.css'

// Containers
import { DefaultLayout } from './containers';
// Pages
import { Login, Page404, Page500, Register } from './views/Pages';


// import { renderRoutes } from 'react-router-config';






const PrivateRoute = ({component: Component, ...rest}) => {
  const tokenCheck = function(){
  //console.log("entered token Check");
  //console.log(sessionStorage.getItem('token'));
  if (sessionStorage.getItem('token') == null){
    //console.log("false");
    return false;
  }
  else{
    //console.log("true");
    return true;
  }
}
  return (
    <Route
      {...rest}

      render={(props) => tokenCheck() === true
        ? <Component {...props} />
        : <Redirect to={{pathname: '/login', state: {from: props.location}}} />} />
  )
}

class App extends Component {
  render() {
    return (
      <HashRouter>
        <Switch>
          <Route exact path="/login" name="Login Page" component={Login} />
          <Route exact path="/register" name="Register Page" component={Register} />
          <Route exact path="/404" name="Page 404" component={Page404} />
          <Route exact path="/500" name="Page 500" component={Page500} />
          <PrivateRoute path='/' component = {DefaultLayout} />
        </Switch>
      </HashRouter>
    );
  }
}

export default App;
