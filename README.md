# README #

This React application represents the front-end part of our DES project.

### How do I get set up? ###

* You will need NodeJS > 8 and Yarn
* After getting the dependencies use: yarn install
* You can use yarn start or yarn build to deploy

